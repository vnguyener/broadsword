import "./main.scss";

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createBrowserHistory } from "history";
import rootStore from "./stores/root.store";

import App from "./app";
export const history = createBrowserHistory({ basename: "/" });

(async () => {
  const initialState = {};
  const store = rootStore(initialState, history);

  const rootEl = document.getElementById("app");
  const render = (Component, el) => {
    ReactDOM.render(
      <Provider store={store}>
        <Component history={history} dispatch={store.dispatch} />
      </Provider>,
      el
    );
  };

  render(App, rootEl);
})(window);
