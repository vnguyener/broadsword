import React from "react";
import PropTypes from "prop-types";
import Icon from "@material-ui/core/Icon";

const FormErrors = ({ error }) => {
  return (
    <div className="form-errors-container">
      <Icon color="error">error_outline</Icon>
      <p>{error}</p>
    </div>
  );
};

FormErrors.propTypes = {
  error: PropTypes.string, // todo - support string or array
};

export default FormErrors;
