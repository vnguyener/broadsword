import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

const PasswordStrength = ({ strength, validation }) => {
  const [feedback, setFeedback] = useState("");

  useEffect(() => {
    switch (strength) {
      case 1:
        setFeedback("Too weak");
        break;
      case 2:
        setFeedback("Could be better");
        break;
      case 3:
        setFeedback("Good password");
        break;
      case 4:
        setFeedback("Strong password");
        break;
      default:
        setFeedback("");
        break;
    }
  }, [strength]);

  return (
    <div className={`password-strength-container strength-${strength || 0}`}>
      <progress value={strength || 0} max="4" />
      <p className="password-feedback">{validation ? validation : feedback}</p>
    </div>
  );
};

PasswordStrength.propTypes = {
  strength: PropTypes.number,
  validation: PropTypes.string,
};

export default PasswordStrength;
