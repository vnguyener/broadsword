import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import Icon from "@material-ui/core/Icon";

import SessionActions from "../../../stores/session/session.actions";

const useStyles = makeStyles(() => ({
  form: {
    display: "flex",
    flexDirection: "column",
    "& > *": {
      margin: "10px",
    },
  },
}));

const LoginPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  // states - where we handle internal component variables
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailValidation, setEmailValidation] = useState(null);
  const [passwordValidation, setPasswordValidation] = useState(null);
  // props - what we get from redux store
  const isSubmitting = useSelector((state) => state.session.isSubmitting);
  const errorMessage = useSelector((state) => state.session.errorMessage);

  const handleLogin = (e) => {
    e.preventDefault();
    if (validateEmail(email) || validatePassword(password)) return;
    dispatch(SessionActions.login(email, password));
  };

  const validateEmail = (input) => {
    let hasError = false;
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!input) {
      setEmailValidation("Please enter an email address.");
      hasError = true;
    } else {
      const isValid = regex.test(String(input).toLowerCase());
      if (!isValid) {
        setEmailValidation("Please enter a valid email address.");
        hasError = true;
      } else {
        setEmailValidation(null);
      }
    }
    return hasError;
  };

  const validatePassword = (input) => {
    let hasError = false;
    if (!input) {
      setPasswordValidation("Please enter a password.");
      hasError = true;
    } else {
      setPasswordValidation(null);
    }
    return hasError;
  };

  const redirectToSignup = () => {
    history.push("/signup");
  };

  const errorAdornment = (
    <InputAdornment position="end">
      <Tooltip title={errorMessage}>
        <Icon color="error">error_outline</Icon>
      </Tooltip>
    </InputAdornment>
  );

  return (
    <Box display="flex" flexDirection="row" className="login-container second">
      <Box className="login-main card shadow-3" justifyContent="flex-start">
        <h1 className="welcome-text sarif">
          Hello, <br />
          Welcome back
        </h1>
        <form
          className={classes.form}
          onSubmit={handleLogin}
          noValidate
          autoComplete="off"
        >
          <TextField
            error={emailValidation ? true : false}
            id="login-email"
            label="Email"
            type="email"
            disabled={isSubmitting}
            required
            InputProps={
              errorMessage
                ? {
                    endAdornment: errorAdornment,
                  }
                : null
            }
            onChange={(e) => {
              const input = e.target.value;
              validateEmail(input);
              setEmail(input);
            }}
            helperText={emailValidation}
          />
          <TextField
            error={passwordValidation ? true : false}
            id="login-password"
            label="Password"
            disabled={isSubmitting}
            type="password"
            required
            onChange={(e) => {
              const input = e.target.value;
              validatePassword(input);
              setPassword(input);
            }}
            helperText={passwordValidation}
          />
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={isSubmitting}
            className="login-btn"
          >
            Sign In
          </Button>
          <span className="text-center">
            Don&apos;t have an account?{" "}
            <span className="link clickable" onClick={redirectToSignup}>
              Join Broadsword
            </span>
          </span>
        </form>
      </Box>
      <Box className="login-hero" flexGrow={1} bgcolor="background.paper"></Box>
    </Box>
  );
};

LoginPage.propTypes = {
  isSubmitting: PropTypes.bool,
  errorMessage: PropTypes.string,
};

export default LoginPage;
