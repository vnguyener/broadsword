import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import zxcvbn from "zxcvbn";

import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

import AccountActions from "../../../stores/account/account.actions";
import PasswordStrength from "../../shared/passwordStength";
import FormErrors from "../../shared/formErrors";

const useStyles = makeStyles(() => ({
  form: {
    display: "flex",
    flexDirection: "column",
    "& > *": {
      margin: "10px",
    },
  },
}));

const SignUpPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  // states - where we handle internal component variables
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [firstNameValidation, setFirstNameValidation] = useState(null);
  const [lastNameValidation, setLastNameValidation] = useState(null);
  const [emailValidation, setEmailValidation] = useState(null);
  const [passwordStrength, setPasswordStrength] = useState(null);
  const [passwordValidation, setPasswordValidation] = useState(null);
  const [confirmValidation, setConfirmValidation] = useState(null);

  // props - what we get from redux store
  const isCreating = useSelector((state) => state.account.isCreating);
  const accountCreateError = useSelector((state) => state.account.errorMessage);
  const handleCreateAccount = (e) => {
    e.preventDefault();

    if (
      validateFirstName(firstName) ||
      validateLastName(lastName) ||
      validateEmail(email) ||
      validatePassword(password) ||
      validateConfirmPassword(confirmPassword)
    )
      return;

    dispatch(
      AccountActions.createNewAccount(firstName, lastName, email, password)
    );
  };

  const validateFirstName = (input) => {
    let hasError = false;
    if (!input) {
      hasError = true;
      setFirstNameValidation("Please enter your first name.");
    } else {
      setFirstNameValidation(null);
    }

    return hasError;
  };

  const validateLastName = (input) => {
    let hasError = false;
    if (!input) {
      hasError = true;
      setLastNameValidation("Please enter your last name.");
    } else {
      setLastNameValidation(null);
    }

    return hasError;
  };

  const validateEmail = (input) => {
    let hasError = false;
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!input) {
      setEmailValidation("Please enter an email address.");
      hasError = true;
    } else {
      const isValid = regex.test(String(input).toLowerCase());
      if (!isValid) {
        setEmailValidation("Please enter a valid email address.");
        hasError = true;
      } else {
        setEmailValidation(null);
      }
    }
    return hasError;
  };

  const validatePassword = (input) => {
    let hasError = false;
    if (!input) {
      hasError = true;
      setPasswordValidation("Please enter a password.");
    } else {
      setPasswordValidation(null);
    }

    const result = zxcvbn(input);
    const solidPasswordScore = 3;
    if (result) {
      setPasswordStrength(result.score);
    }

    return !result || (result && result.score < solidPasswordScore) || hasError;
  };

  const validateConfirmPassword = (input) => {
    let hasError = false;
    if (!input) {
      hasError = true;
      setConfirmValidation("Please confirm your password.");
    } else if (input !== password) {
      hasError = true;
      setConfirmValidation("Your passwords do not match");
    } else {
      setConfirmValidation(null);
    }

    return hasError;
  };

  const redirectToSignup = () => {
    history.push("/login");
  };

  return (
    <Box display="flex" flexDirection="row" className="login-container">
      <Box className="login-main card shadow-3" justifyContent="flex-start">
        <h1 className="welcome-text sarif text-center">Join Broadsword</h1>
        {accountCreateError && <FormErrors error={accountCreateError} />}
        <form
          className={classes.form}
          onSubmit={handleCreateAccount}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="signup-first"
            label="First Name"
            disabled={isCreating}
            required
            error={firstNameValidation ? true : false}
            onChange={(e) => {
              const input = e.target.value;
              validateFirstName(input);
              setFirstName(input);
            }}
            helperText={firstNameValidation}
          />
          <TextField
            id="signup-last"
            label="Last Name"
            disabled={isCreating}
            required
            onChange={(e) => {
              const input = e.target.value;
              validateLastName(input);
              setLastName(input);
            }}
            error={lastNameValidation ? true : false}
            helperText={lastNameValidation}
          />
          <TextField
            id="signup-email"
            label="Email"
            disabled={isCreating}
            required
            onChange={(e) => {
              const input = e.target.value;
              validateEmail(input);
              setEmail(input);
            }}
            error={emailValidation ? true : false}
            helperText={emailValidation}
          />
          <TextField
            id="signup-password"
            label="Password"
            disabled={isCreating}
            type="password"
            required
            onChange={(e) => {
              const input = e.target.value;
              validatePassword(input);
              setPassword(input);
            }}
            error={passwordValidation ? true : false}
            helperText={passwordValidation}
          />
          {password && password.length > 0 && (
            <PasswordStrength strength={passwordStrength} />
          )}
          <TextField
            id="signup-confirmpasswprd"
            label="Confirm Password"
            disabled={isCreating}
            type="password"
            required
            onChange={(e) => {
              const input = e.target.value;
              setConfirmPassword(input);
            }}
            error={confirmValidation ? true : false}
            helperText={confirmValidation}
          />
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={isCreating}
            className="signup-btn"
          >
            Sign Up
          </Button>
          <span className="text-center">
            Already a member?{" "}
            <span className="link clickable" onClick={redirectToSignup}>
              Sign in
            </span>
          </span>
        </form>
      </Box>
      <Box className="login-hero" flexGrow={1} bgcolor="background.paper"></Box>
    </Box>
  );
};

SignUpPage.propTypes = {
  isCreating: PropTypes.bool,
};

export default SignUpPage;
