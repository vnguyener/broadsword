import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import SessionActions from "../../stores/session/session.actions";

const DashboardPage = () => {
  const dispatch = useDispatch();
  const { email } = useSelector((state) => state.session.sessionData || {});

  useEffect(() => {
    dispatch(SessionActions.getSessionData());
  }, []);

  return (
    <>
      <p>Dashboard</p>
      <p>something something welcome {email}</p>
    </>
  );
};

export default DashboardPage;
