import React, { Suspense, lazy } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { ConnectedRouter } from "connected-react-router";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";
import blueGrey from "@material-ui/core/colors/blueGrey";

const LoginPage = lazy(() => import("./components/session/login/login"));
const SignUpPage = lazy(() => import("./components/session/signup/signup"));
const AccountPage = lazy(() => import("./components/account/account"));
const DashboardPage = lazy(() => import("./components/dashboard/dashboard"));

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: blueGrey,
  },
});

const App = (props) => {
  return (
    <ConnectedRouter history={props.history}>
      <Suspense fallback={<div>Loading...</div>}>
        <ThemeProvider theme={theme}>
          <Switch>
            <Route exact path="/">
              <Redirect to="/login" />
            </Route>
            <Route exact path="/login" component={LoginPage} />
            <Route exact path="/dashboard" component={DashboardPage} />
            <Route exact path="/account" component={AccountPage} />
            <Route exact path="/signup" component={SignUpPage} />
          </Switch>
        </ThemeProvider>
      </Suspense>
    </ConnectedRouter>
  );
};

App.propTypes = {
  history: PropTypes.object,
};

export default App;
