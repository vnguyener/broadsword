import BaseReducer from "../base.reducer";
import AccountActions from "./account.actions";

export default class AccountReducer extends BaseReducer {
  initialState = {
    isSubmitting: false,
    isSuccessful: false,
    errorMessage: "",
  };

  [AccountActions.ACCOUNT_CREATE_STATE](state, action) {
    return {
      ...state,
      isSubmitting: action.payload.submitting,
    };
  }

  [AccountActions.ACCOUNT_CREATE_SUCCESS](state, action) {
    return {
      ...state,
      isSuccessful: action.payload.success,
    };
  }

  [AccountActions.ACCOUNT_CREATE_ERROR](state, action) {
    return {
      ...state,
      errorMessage: action.payload.errorMessage,
    };
  }
}
