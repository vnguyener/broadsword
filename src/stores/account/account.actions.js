import axios from "axios";

export default class AccountActions {
  static ACCOUNT_CREATE_STATE = "ACCOUNT_CREATE_STATE";
  static ACCOUNT_CREATE_ERROR = "ACCOUNT_CREATE_ERROR";
  static ACCOUNT_CREATE_SUCCESS = "ACCOUNT_CREATE_SUCCESS";

  static toggleAccountCreateState = (isSubmitting) => ({
    type: AccountActions.ACCOUNT_CREATE_STATE,
    payload: {
      submitting: isSubmitting,
    },
  });

  static toggleAccountCreateSuccess = (isSuccessful) => ({
    type: AccountActions.ACCOUNT_CREATE_SUCCESS,
    payload: {
      success: isSuccessful,
    },
  });

  static setAccountCreateError = (errorMessage) => ({
    type: AccountActions.ACCOUNT_CREATE_ERROR,
    payload: {
      errorMessage,
    },
  });

  static createNewAccount = (firstName, lastName, email, password) => {
    return async (dispatch) => {
      try {
        const response = await axios.post("/api/account", {
          firstName,
          lastName,
          email,
          password,
        });

        dispatch(AccountActions.toggleAccountCreateState(false));

        if (response && response.data && response.success) {
          dispatch(AccountActions.toggleAccountCreateSuccess(true));
        }
      } catch (error) {
        if (
          error &&
          error.response &&
          error.response.data &&
          error.response.data.message
        ) {
          const errorResponse = error.response.data.message;
          if (typeof errorResponse == "string") {
            dispatch(
              AccountActions.setAccountCreateError(
                errorResponse || "Unexpected error has occured."
              )
            );
          } else if (Array.isArray(errorResponse)) {
            // todo - iterate and set for each
            dispatch(
              AccountActions.setAccountCreateError(
                errorResponse[0].msg || "Unexpected error has occured."
              )
            );
          }
        }
        dispatch(AccountActions.toggleAccountCreateState(false));
      }
    };
  };
}
