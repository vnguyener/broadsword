import axios from "axios";
import { history } from "../../index";

export default class SessionActions {
  static LOGIN_STATE = "LOGIN_STATE";
  static LOGIN_ERROR = "LOGIN_ERROR";
  static SET_SESSION_DATA = "SET_SESSION_DATA";

  static toggleLoginState = (isSubmitting) => ({
    type: SessionActions.LOGIN_STATE,
    payload: {
      submitting: isSubmitting,
    },
  });

  static setLoginError = (errorMessage) => ({
    type: SessionActions.LOGIN_ERROR,
    payload: {
      errorMessage,
    },
  });

  static setSessionData = (sessionData) => ({
    type: SessionActions.SET_SESSION_DATA,
    payload: {
      session: sessionData,
    },
  });

  static getSessionData = () => {
    return async (dispatch) => {
      try {
        const response = await axios.get("/api/session");
        dispatch(SessionActions.setSessionData(response.data.sessionData));
      } catch (error) {
        // we'll do this for now if we can't get session data
        history.push("/login");
      }
    };
  };

  static login = (email, password) => {
    return async (dispatch) => {
      dispatch(SessionActions.toggleLoginState(true));
      dispatch(SessionActions.setLoginError(""));

      try {
        const response = await axios.post("/api/session/auth", {
          email,
          password,
        });

        dispatch(SessionActions.toggleLoginState(false));

        if (response && response.data && response.data.success) {
          history.push("/dashboard");
        }
      } catch (error) {
        if (
          error &&
          error.response &&
          error.response.data &&
          error.response.data.message
        ) {
          const errorResponse = error.response.data.message;
          if (typeof errorResponse == "string") {
            dispatch(
              SessionActions.setLoginError(
                errorResponse || "Unexpected error has occured."
              )
            );
          } else if (Array.isArray(errorResponse)) {
            // todo - iterate and set for each
            dispatch(
              SessionActions.setLoginError(
                errorResponse[0].msg || "Unexpected error has occured."
              )
            );
          }
        }
        dispatch(SessionActions.toggleLoginState(false));
      }
    };
  };
}
