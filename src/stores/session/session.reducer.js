import BaseReducer from "../base.reducer";
import SessionActions from "./session.actions";

export default class SessionReducer extends BaseReducer {
  initialState = {
    isSubmitting: false,
    errorMessage: "",
    sessionData: null,
  };

  [SessionActions.LOGIN_STATE](state, action) {
    return {
      ...state,
      isSubmitting: action.payload.submitting,
    };
  }

  [SessionActions.LOGIN_ERROR](state, action) {
    return {
      ...state,
      errorMessage: action.payload.errorMessage,
    };
  }

  [SessionActions.SET_SESSION_DATA](state, action) {
    return {
      ...state,
      sessionData: action.payload.session,
    };
  }
}
