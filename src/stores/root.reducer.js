import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import SessionReducer from "./session/session.reducer";
import AccountReducer from "./account/account.reducer";

export default (history) => {
  const reducerMap = {
    session: new SessionReducer().reducer,
    account: new AccountReducer().reducer,
    router: connectRouter(history),
  };

  return combineReducers(reducerMap);
};
