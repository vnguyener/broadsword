const passport = require("passport");
const localPassport = require("passport-local");
const models = require("./models");

const LocalStrategy = localPassport.Strategy;
const { User } = models;

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
    },
    async (email, password, done) => {
      try {
        const dbUser = await User.findOne({
          where: {
            email,
          },
        });

        if (!dbUser) {
          return done(null, false, {
            message:
              "The email address you’ve entered doesn’t match any account.",
          });
        }

        const isPasswordValid = await dbUser.validPassword(password);

        if (!isPasswordValid) {
          return done(null, false, {
            message:
              "The email address and/or password you’ve entered doesn’t match any account.",
          });
        }

        return done(null, dbUser);
      } catch (err) {
        return done(null, false, {
          message: "Server Error.",
        });
      }
    }
  )
);

passport.serializeUser((user, cb) => {
  cb(null, user);
});

passport.deserializeUser((obj, cb) => {
  cb(null, obj);
});

module.exports = passport;
