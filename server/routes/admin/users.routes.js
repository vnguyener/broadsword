"use strict";

const express = require("express");
const router = express.Router();
const models = require("../../models");

/*
 *   GET: "/users" - list all users
 */

// routes
router.get("", getUsers);

async function getUsers(req, res) {
  const users = await models.User.findAll();

  res.status(200).send({
    title: "does i has anys users",
    users,
  });
}

module.exports = router;
