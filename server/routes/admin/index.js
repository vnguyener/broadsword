/**
 *
 */

const MainRouter = require("express").Router();

MainRouter.use("/users", require("./users.routes"));

module.exports = MainRouter;
