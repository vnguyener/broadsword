"use strict";

const express = require("express");
const { body } = require("express-validator");
const httpStatus = require("http-status");
const passport = require("../../passport");
const router = express.Router();
const { sessionMiddleware } = require("./main.middlewares");

///
//  GET: "/" - get session data
//  POST: "/auth" - login/authentication, creates a user session
///

// routes
router.post(
  "/auth",
  [body("email").exists().isEmail(), body("password").exists()],
  sessionMiddleware.auth,
  auth
);

router.get("", sessionMiddleware.getSessionData, getSessionData);

///
// Login
///
async function auth(req, res, next) {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(httpStatus.UNAUTHORIZED).json({
        ...info,
        success: false,
        httpStatus: httpStatus.UNAUTHORIZED,
      });
    } else {
      // set session email
      const { email } = req.body;
      const session = req.session;
      session.email = email;

      res
        .status(httpStatus.OK)
        .json({ success: true, httpStatus: httpStatus.OK });
    }
  })(req, res, next);
}

///
// WIP: Returns session data we need once we're authenticated and in the app
///
async function getSessionData(req, res) {
  const session = req.session;
  if (session.email) {
    res.status(httpStatus.OK).json({
      success: true,
      httpStatus: httpStatus.OK,
      sessionData: {
        email: session.email,
      },
    });
  } else {
    res.status(httpStatus.UNAUTHORIZED).json({
      success: false,
      httpStatus: httpStatus.UNAUTHORIZED,
      redirect: true,
    });
  }
}

module.exports = router;
