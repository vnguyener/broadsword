/**
 *
 */

const MainRouter = require("express").Router();

MainRouter.use("/session", require("./session.routes"));
MainRouter.use("/account", require("./account.routes"));

module.exports = MainRouter;
