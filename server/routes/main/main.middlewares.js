const authenticateMiddleware = require("../../middleware/authenticate");
const paramsMiddleware = require("../../middleware/params");

const accountMiddleware = {
  createNewAccount: [paramsMiddleware],
};

const sessionMiddleware = {
  getSessionData: [authenticateMiddleware],
  auth: [paramsMiddleware],
};

module.exports = {
  accountMiddleware,
  sessionMiddleware,
};
