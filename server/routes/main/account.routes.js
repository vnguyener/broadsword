"use strict";

const express = require("express");
const httpStatus = require("http-status");
const zxcvbn = require("zxcvbn");
const { body } = require("express-validator");
const models = require("../../models");
const { accountMiddleware } = require("./main.middlewares");

const { User } = models;
const router = express.Router();

///
//   POST: "" - create new account
///

router.post(
  "/",
  [
    body("firstName").exists(),
    body("lastName").exists(),
    body("email").exists().isEmail(),
    body("password").exists(),
  ],
  accountMiddleware.createNewAccount,
  createNewAccount
);

///
// Creates a new user account, checks for existing accounts or bad inputs first
///
async function createNewAccount(req, res) {
  const { password, email, firstName, lastName } = req.body;
  const result = zxcvbn(password);
  const solidPasswordScore = 3;
  if (result.score < solidPasswordScore) {
    return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({
      success: false,
      httpStatus: httpStatus.UNPROCESSABLE_ENTITY,
      message: "Please enter a valid password.",
    });
  }

  const user = await User.findOne({
    where: {
      email,
    },
  });

  if (user) {
    return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({
      success: false,
      httpStatus: httpStatus.UNPROCESSABLE_ENTITY,
      message:
        "There is already an account associated with this email address.",
    });
  } else {
    await User.create({
      firstName,
      lastName,
      email,
      password,
      verified: false,
      isAdmin: false,
      lastLoginTime: new Date(),
    });
  }

  res.status(httpStatus.OK).json({
    success: true,
    httpStatus: httpStatus.OK,
  });
}

module.exports = router;
