"use strict";
const mainRoutes = require("./main");
const adminRoutes = require("./admin");

function Routes(app) {
  app = app || {};
  // const env = app.get('env');

  // for loading up the front-end
  app.get("/", (req, res) => {
    res.sendFile("index.html", { root: "public" });
  });

  /**
   * /main - app routes
   * /admin - admin routes
   *
   * @description app.use("/path", dirPath) specifies the endpoint/route that points
   * to an internal directory that houses a separate JS file with the specific,
   * defined route logic.
   */

  // Route and directory for 'main' api routes
  app.use("/api", mainRoutes);
  // Route and directory for 'admin' api routes
  app.use("/admin", adminRoutes);
}

module.exports = Routes;
