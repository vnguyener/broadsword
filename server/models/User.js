const bcrypt = require("bcryptjs");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      verified: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0,
      },
      isAdmin: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0,
      },
      lastLoginTime: {
        type: DataTypes.TIME,
        allowNull: true,
      },
    },
    {
      hooks: {
        // Before a User is created, we will automatically hash their password
        beforeCreate: (user) => {
          user.password = bcrypt.hashSync(
            user.password,
            bcrypt.genSaltSync(10),
            null
          );
        },
      },
    }
  );

  // Compares the provided password with the salted version to determine if it's the correct password
  User.prototype.validPassword = function (password) {
    return bcrypt.compare(password, this.password);
  };

  return User;
};
