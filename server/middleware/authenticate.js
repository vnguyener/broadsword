const models = require("../models");
const { User } = models;

const authenticateMiddlware = async (req, res, next) => {
  // check if user exists else throw 401
  if (req.session && req.session.email) {
    const user = await User.findOne({
      where: {
        email: req.session.email,
      },
    });

    if (!user) {
      res.status(401);
      next();
    }

    return next();
  }

  res.status(401);
  next();
};

module.exports = authenticateMiddlware;
