"use strict";

const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const cookieParser = require("cookie-parser");
const expressSession = require("express-session");
const redis = require("redis");
const cors = require("cors");
const helmet = require("helmet");

const models = require("./models");
const { logger, log } = require("./logger");

const redisStore = require("connect-redis")(expressSession);
const redisClient = redis.createClient();

redisClient.on("error", (err) => {
  // eslint-disable-next-line no-console
  console.log("Redis:error - ", err);
});

const server = (config) => {
  config = config || {};

  let self = {};

  self.log = (message) => {
    log("info", message);
  };

  self.error = (message) => {
    log("error", message);
  };

  self.start = async () => {
    // configuration stuffs
    self.express = express();
    self.express.use(bodyParser.urlencoded({ extended: true }));
    self.express.use(bodyParser.json());
    self.express.use(methodOverride("X-HTTP-Method-Override"));
    self.express.use(cookieParser());
    self.express.use(express.static("public"));
    self.express.use(helmet());
    self.express.use(cors());

    // set logging
    self.express.use(
      morgan(
        ":date[iso] [HTTP REQUEST] :url :method :status :res[content-length] - :response-time ms",
        {
          stream: logger.stream.write,
          skip: () => process.env.NODE_ENV === "test",
        }
      )
    );

    // redis/session
    self.express.use(
      expressSession({
        secret: config.redis.secret,
        name: config.redis.name,
        resave: true,
        saveUninitialized: true,
        cookie: { secure: false, maxAge: 60000 * 10 }, // maxAge in milli
        store: new redisStore({
          host: "localhost",
          port: 6379,
          client: redisClient,
          ttl: 1440,
        }), // ttl in seconds
      })
    );

    // Start the server listening
    config.port = config.port || process.env.port || 8080;
    const instance = self.express.listen(config.port);

    // sync models
    await models.sequelize.sync();

    //configure routes
    require("./routes")(self.express);

    self.log("magic happens on localhost:" + config.port);

    process.on("SIGTERM", () => {
      self.log("Closing server.");
      instance.close(() => {
        self.log("Server closed.");
        process.exit(0);
      });
    });

    return instance;
  };

  return self;
};

server.start = async (config) => {
  const instance = server(config);
  await instance.start();
  return instance;
};

module.exports = server;
