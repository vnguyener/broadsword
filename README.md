[![Build Status](http://23.23.74.74:8080/buildStatus/icon?job=broadsword_github)](http://23.23.74.74:8080/job/broadsword_github/)

## Getting Started
### Prerequisites
 - git
 - yarn
 - mysql & a mysql gui of your choice e.g. mysqlworkbench or sequelize
 - redis & a redis gui of your choice e.g. redisinsight or medis

## Installing
- `yarn install`
- Make sure you create a `config.json` under `/config` e.g.
```
{
    "host": "127.0.0.1",
    "port": 8080,  // or whatever port you want to run on, make sure this is the same as our proxy target port in our webpack config
    "sequelize": {
      "username": "root",
      "password": "password",
      "database": "bs_dev"
    },
    "mysql": {
      "dialect": "mysql",
      "host": "localhost",
      "port": 8081 // or whatever port you want to run on
    },
    "redis": {
      "secret": "myredissecret", // redis secret for the express session, can be whatever
      "name": "bs_dev"
    }
  }
```
- To run locally, we want to run two terminals for `yarn server` (our server) and `yarn dev` (our client)

## Frontend Architecture
### Overview
Our frontend is mainly comprised of `react` w/ `redux` with `material-ui` sprinkled on top to make things look nice.
### Structure
```
/src
  /assets // pictures, icons, fonts and things
  /components // these are separated by 'pages' of the app, each have their own scss if needed and component
    /login
      login.js/.jsx
      login.scss
    /shared  // shared can be comprised of navs, tables, cards, etc. if it's used in more than a couple places through the app
  ...
  /constants // app level hard codes things we dont want to look at
  /selectors // custom selectors
  /stores // the redux for our app, separated by action context aka what it's for, each folder has its own actions/reducers to keep things easy
    /session
      session.actions.js
      sessions.reducers.js
  app.js // where main routing lives
  index.js // typical base react component
  index.scss // any global level styles
```

## Middleware/Server Architecture

### Overview
Typical express/node backend
### Structure
```
/server
  /logs - our error logs
  /middleware
    authenticate.js -- currently pretty naive, checks if theres a session object in the req and an email, if so validate/try to find a user with that email of return if so
    params.js -- abstracted way to check express-validator rules in our routes
  /models
    index.js
    User.js - typical Sequelize model
  /routes // our routes are split up between apis for our main app, admin, and possible api routes to expose
    index.js
    /main // actual app routes - account, session, dashboard, etc
      account.routes.js // account related api routes
      index.js // our "router wrapper"
      main.middlewares.js // object to store sets of middlewares for different routes
    /admin // admin functionality routes to manage app things
  index.js // our express wrapper, and where we can put any middleware
  passport.js // passport configuration for our passport middleware
  logger.js // our winston logger setup
```

## Database(s)
- Sequelize - metadata, user data, etc
- Redis - for sessions and other things

## Stack
- Node
- Express
- React/Redux
- Sequelize
- Redis
- Jenkins
