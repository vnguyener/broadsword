"use strict";

const server = require("./server");
const env = process.env.NODE_ENV || "development";
const config = require("./config/config.json")[env];

server.start(config);
