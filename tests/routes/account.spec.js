const nock = require("nock");
const axios = require("axios");

axios.defaults.adapter = require("axios/lib/adapters/http");

describe("account", () => {
  it("should pong when i ping", async () => {
    const scope = nock("http://localhost:8080")
      .get("/main/account/ping")
      .reply(200, "pong");

    const res = await axios.get("http://localhost:8080/main/account/ping");
    expect.objectContaining({
      status: expect.any(Number),
      data: expect.any(String),
    });
    expect(res.status).toBe(200);
    expect(res.data).toBe("pong");
    scope.done();
  });
});
